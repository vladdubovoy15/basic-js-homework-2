"use strict";

// First Task

let myNumber;

do {
    myNumber = prompt("Enter a random integer number");
} while (myNumber === null || myNumber === "" || isNaN(+myNumber) || Number.isInteger(+myNumber) === false);

if (myNumber > 0) {
    for (let i = 0; i <= myNumber; i++) {
        if ( i % 5 === 0) {
            console.log(i) 
        }  
    }  
} else console.log("Sorry! No numbers");


// Second Task 

let firstNumber, secondNumber;

do {
firstNumber = +prompt("Enter the first number which is greater than 1");
secondNumber = +prompt("Enter the second number which is greater than the first one");
} while (firstNumber > secondNumber ||
         firstNumber === null || 
         firstNumber === "" ||
         isNaN(firstNumber) || 
         secondNumber === null || 
         secondNumber === "" ||
         isNaN(secondNumber) ||
         firstNumber <= 1
         );

nextPrime:
for (let i = firstNumber; i <= secondNumber; i++) {
    for (let j = 2; j < i; j++) {
        if(i % j == 0) {
            continue nextPrime;
        }
    }
       console.log(i);
   }
    


